<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MoviesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/movies', [MoviesController::class, 'index']);
Route::put('/movies/add/{id}', [MoviesController::class, 'add']);
Route::put('/movies/remove/{id}', [MoviesController::class, 'remove']);
Route::get('/user', [MoviesController::class, 'authenticate']);

