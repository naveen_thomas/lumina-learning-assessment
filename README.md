# Lumina Learning Ltd - Take Home Assignment
This is a simple application to view, add and remove favourite movies.
- User's are able to see everyone's Favourite movies
- Authenticated User is able to add movie to favourites.
- Authenticated User is able to remove movie from favourites.
- Authenticated User won't be able to add the same movie again to favourites.

# Assumptions
Used a single table for simplicity.

Used a single page to list all movies, add a movie and remove a movie.

Used 4 API end points in this application:
  - index - To display favourite movies of every user.
  - add - To add a movie to favourite's.
  - remove - To remove a move from favourite's.
  - authenticate - To get the current authenticated user.

## Technology  
### PHP version : 7.3.11
### LARAVEL version : 8.31.0 
### REACT version : 16.2.0
### MySQL version : 8.0.23

## Solution
- Created a new application using laravel and changed the default Vue scaffolding to ReactJS
- Created an end point index to display all movies.
- Created a service to handle OMDB api calls.
- Made use of the service to iterate through each user and get the favourite movies.
- Iterated each imdbID's and called the OMDB api, and returned a json response.
- Created a react component and rendered a movie grid showing details from the response received.
- Created a modal component to display more details when a user clicks on a particular movie.
- Created an end point to add a movie to favourites, when a user clicks on add, movie is added to the array with user id being updated.
- Created an end point to remove a movie from favourites, when a user clicks on remove, movie is removed from array.
- Created an end point to get the authenticated user in the front end to display add/remove buttons accordingly.
- Used default authentication module of Laravel to authenticate users.
- Favourite movies of a user will have a star icon under the movie. 

## Database creation
Create a database in the MySQL server.

Use command **create database _DB_NAME_;** in MySQL(console or workbench)

eg: **create database movielist** creates a database with the name **movielist**.

## Database Initialization
Create a replica of the file .env.example in this project and name it as .env and save it.

Add the name of the created database and credentials of MySQL in the following snippet in the file

`
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=movielist
DB_USERNAME=root
DB_PASSWORD=
` 

Use **php artisan migrate** to run migration

## DB Seed
Use **php artisan db:seed** to populate data in the tables

## Test
Tests are written using PHPUnit and it can be run by using **php artisan test**

## Run project

To run the application,

Firstly use **npm install** to install packages and dependencies

Then, use **php artisan serve** to start the server

## Next steps
- Add additional feature tests using PHPunit 
- Add tests for react using jest and react testing library
- Add filters so as to sort movies according to ratings, name, year..etc
- Add search bar functionality to search movies
