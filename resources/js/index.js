import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import '../css/movie.css';
import '../css/modal_details.css';

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}
