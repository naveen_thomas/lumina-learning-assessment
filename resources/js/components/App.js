import axios from 'axios';
import React from 'react';
import { useEffect, useState } from 'react';
import MovieDetailsModal from './MovieDetailsModal'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const App = () => {

	const [movies, setMovies] = useState([]);
	const [showModal, setShow] = useState(false);
	const [user, setUser] = useState(false);

	const handleClose = () => setShow(false);

	const openModal = (id) =>  {
		setShow({[id]: true})
	 }

	 /* Call movies api on page load and store response in movies state variable*/
	useEffect(() => {
		axios.get(`http://localhost:8000/movies`)
			.then(response => {
				
				const movies = response.data;
				setMovies(movies);
			})
	}, [])

	/* Call user api on page load to see if user is authenticated or not*/
	useEffect(() => {
		axios.get(`http://localhost:8000/user`)
			.then(response => {
				if (Object.keys(response.data).length == 0){
					setUser(false);
				} else {
                    setUser(response.data);
				}
			})
	}, [])

	// Remove movie from favourites
    const handleRemove = (e) => {
	  let imdbID =  e.target.parentNode.getAttribute("data-id");
	  // Get the movie which is clicked
	  let filteredArray = movies.filter(movie => movie.imdbID == imdbID && user.id == movie.user_id);
	  // Remove the particular movie from movies
	  let updatedArray = movies.filter(i => filteredArray.findIndex(f => f.title === i.title && f.user_id === i.user_id));
	  // Call remove api to remove the movie from database
	  axios.put(`/movies/remove/${imdbID}`)
			.then((response) => {
				setMovies(updatedArray); // Setting the updated array when response is success
				toast.success("Removed from favourites");
			})
			.catch((error) => {
				toast.error(error.response.data.message);
			});
	}

	// Add movie to favourites
	const handleAdd = (e) => {
		let imdbID =  e.target.parentNode.getAttribute("data-id");
		// Get the movie which is clicked
		let filteredArray = movies.filter(movie => movie.imdbID == imdbID);
		// Update the user id with the current user's id 
		let updatedArray = [{...filteredArray[0], user_id: user.id}];
		// Call add api to add the movie to the database
		axios.put(`/movies/add/${imdbID}`)
		.then((response) => {
			setMovies([...updatedArray, ...movies ]); // Updating the movies array with the added movie when response is success
			toast.success("Added to favourites");
		})
		.catch((error) => {
			toast.error(error.response.data.message);
		});
	}

	return (
		<div className="container">
			<div className="row justify-content-center">

				{movies.map((movie, index) =>
					<div key={index} id='m-container' className='browse-movie-wrap col-md-3 movie-col'>
						<a className='browse-movie-link' onClick={() => openModal(index)}>
							<figure>
								<img id="img-responsive" className='figure animated fadeIn' src={movie.poster} style={{ height: "288px", width: "198px" }}></img>
								<figcaption>
									<span className='imdb'><i className='fa fa-star' style={{ color: "#f5de50" }}></i></span>
									<div className='rating' data-rating=''>{movie.imdbRating}</div>
									<div className='genre'>{movie.genre}</div>
									<span className='button-green-download-big'>View Details</span>
								</figcaption>
							</figure>
						</a>
						<div className='movie-bottom'>
							<div className='browse-movie-title'>{movie.title}</div>
							<div className='movie-year'>{movie.year}</div>
						</div>
						{ user && <div data-id={movie.imdbID}>
						  { user.id != movie.user_id && <button className="btn btn-sm btn-success" onClick={handleAdd}>Add</button> }
						  { user.id == movie.user_id && 
						   <>
						    <button className="btn btn-sm btn-danger" onClick={handleRemove}>Remove</button>
						    <i className='fa fa-star' style={{ color: "#f5de50", marginLeft: '10px' }}></i>
						   </>
						   }
						  <ToastContainer autoClose={2000} />
						</div> }

						<MovieDetailsModal
							show={showModal[index]}
							title={movie.title}
							plot={movie.plot}
							genre={movie.genre}
							actors={movie.actors}
							director={movie.director}
							language={movie.language}
							year={movie.year}
							poster={movie.poster}
							imdbRating={movie.imdbRating}
							imdbID={movie.imdbID}
							onHide={handleClose} />
					</div>

				)}
			</div>
		</div>
	);
}

export default App;
