import React from 'react';

import { Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const MovieDetailsModal = (props) => {
    return (

        <div>
            <Modal show={props.show} onHide={props.onHide}>

                <Modal.Header closeButton>
                    <Modal.Title>
                        {props.title}
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="row">
                        <div className="col-lg-5">
                            <img style={{width: "100%"}} className="poster" src={props.poster}></img>
                        </div>

                        <div className="col-lg-7" style={{textAlign: "left"}}>
                            <div className="imdb_r">
                                <div className="a">
                                    <span>{props.imdbRating}</span>
                                </div>
                                <div className="b">
                                    <div className="bar"><span style={{width: props.imdbRating*10 + "%"}}></span></div>
                                    <span className="dato"><b>IMDB: {props.imdbRating}/10</b></span>
                                </div>
                            </div>
                            <p><strong>Genre:</strong>&nbsp;<span>{props.genre}</span></p>
                            <p><strong>Language:</strong>&nbsp;<span>{props.language}</span></p>
                            <p><strong>Director:</strong>&nbsp;<span>{props.director}</span></p>
                            <p><strong>Cast:</strong>&nbsp;<span>{props.actors}</span></p>
                            <p>{props.plot}</p>
                            <p><a id="movie_209112" className="result" href={`http://www.imdb.com/title/${props.imdbID}`} target='_blank'>More Info</a></p>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    )
};


export default MovieDetailsModal;
