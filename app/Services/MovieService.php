<?php

namespace App\Services;
use Http;

class MovieService
{
    public function displayFavouriteMovies($users)
    {
        $omdb_key = env('OMDB_API_KEY');
        foreach ($users as $user){
            // skip to next user if user doesn't have any movies added to favourites
            if (empty($user->favourite_movies))
              continue;
            $imdbIDs = explode(',', $user->favourite_movies);
            foreach ($imdbIDs as $imdbID) {
                $response = Http::get("http://www.omdbapi.com/?apikey=$omdb_key&i=$imdbID");
                // construct json array
                $fav_movies[] = [
                    'title' => $response->json()['Title'],
                    'plot' => $response->json()['Plot'],
                    'poster' => $response->json()['Poster'],
                    'year' => $response->json()['Year'],
                    'actors' => $response->json()['Actors'],
                    'director' => $response->json()['Director'],
                    'genre' => $response->json()['Genre'],
                    'language' => $response->json()['Language'],
                    'imdbRating' => $response->json()['imdbRating'],
                    'imdbID' => $imdbID,
                    'user_id' => $user->id,
                ];
            }  
        }
        return $fav_movies;
    }
}