<?php

namespace App\Http\Controllers;
use DB;
use Http;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\MovieService;

class MoviesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'authenticate']);
    }
    
    // Display favourite movies of every user
    public function index()
    {
        $users = User::all();
        $fav_movies = (new MovieService)->displayFavouriteMovies($users);
        return response()->json($fav_movies);
    }

    // Add movie to favourites
    public function add(Request $request, $imdbID)
    {
        $user = auth()->user();
        $imdbIDs = [];
        if (!empty($user->favourite_movies)) {
          $imdbIDs = explode(',', $user->favourite_movies);
          // Display error notification if the favourite movie already exists for that user
          if (in_array($imdbID, $imdbIDs))
            abort(403, 'Already marked as favourite!');
        }
        array_push($imdbIDs, $imdbID);
        $updated_imdbIDs = implode(',',$imdbIDs);
        $user->update(['favourite_movies' => $updated_imdbIDs]);
        return response()->json($user);
    }

    // Remove movie from favourites
    public function remove(Request $request, $imdbID)
    {
        $user = auth()->user();
        $imdbIDs = explode(',', $user->favourite_movies);
        $index = array_search($imdbID,$imdbIDs);
        array_splice($imdbIDs, $index, 1);
        $updated_imdbIDs = implode(',',$imdbIDs);
        $user->update(['favourite_movies' => $updated_imdbIDs]);
        return response()->json($user);
    }

    // Get authenticated user
    public function authenticate() {
        $user = auth()->user();
        return response()->json($user);
    }
}
