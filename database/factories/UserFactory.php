<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "id"=> 1,
            "firstName"=> "Anona",
            "lastName"=> "Cruz",
            "email"=> "anona@gmail.com",
            "email_verified_at"=> null,
            "favourite_movies"=> "tt4154756",
            "created_at"=> "2021-03-09T15:09:58.000000Z",
            "updated_at"=> "2021-03-14T17:41:49.000000Z"
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
