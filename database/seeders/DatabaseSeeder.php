<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['firstName' => 'Anona', 'lastName' => 'Cruz', 'email' => 'anona@gmail.com', 'password' => bcrypt('anona123'), 'favourite_movies' => 'tt0848228,tt4154756,tt2395427,tt4154796'],
            ['firstName' => 'Camilla', 'lastName' => 'Sayer', 'email' => 'camilla@gmail.com', 'password' => bcrypt('camilla123'), 'favourite_movies' => 'tt4154756,tt10515848,tt0120575'],
            ['firstName' => 'Ganesh', 'lastName' => 'Zentai', 'email' => 'ganesh@gmail.com', 'password' => bcrypt('ganesh1231243'), 'favourite_movies' => 'tt0287871,tt2975590,tt0103776,tt4116284,tt2313197'],
            ['firstName' => 'Vivien', 'lastName' => 'Straub', 'email' => 'vivien@gmail.com', 'password' => bcrypt('vivien1231243'), 'favourite_movies' => 'tt0926084,tt0417741'],
            ['firstName' => 'Bernardita', 'lastName' => 'Bishop', 'email' => 'bernardita@gmail.com', 'password' => bcrypt('bernardita123'), 'favourite_movies' => 'tt0389860'],
        ];

        foreach ($users as $user){
            User::create($user); 
        }
        dd("Seeded successfully!!");
    }
}
