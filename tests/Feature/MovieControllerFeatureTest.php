<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class MovieControllerFeatureTest extends TestCase
{
    public function test_user_can_login_with_correct_credentials()
    {
        $user = User::factory()->make([
            'password' => bcrypt($password = 'anona123'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
    }

    public function test_display_favourite_movies()
    {
        $response = $this->call('GET', 'movies');
        $response->assertStatus(200)->assertJsonFragment([
            "title" => "Avengers: Infinity War",
            "plot"=> "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.",
            "poster"=> "https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_SX300.jpg",
            "year"=> "2018",
            "actors"=> "Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans",
            "director"=> "Anthony Russo, Joe Russo",
            "genre"=> "Action, Adventure, Sci-Fi",
            "language"=> "English",
            "imdbRating"=> "8.4",
            "imdbID"=> "tt4154756",
        ]);
    }
}
