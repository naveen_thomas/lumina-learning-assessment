<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Services\MovieService;

class MovieControllerUnitTest extends TestCase
{

    public function test_it_returns_200_status_for_index_method()
    {
        $response = $this->call('GET', 'movies');
        $response->assertStatus(200);
    }

    public function test_it_returns_302_status_for_add_method_when_user_is_not_authenticated()
    {
        $response = $this->call('PUT', "/movies/add/tt10515848");
        $response->assertStatus(302);
    }

    public function test_it_returns_200_status_for_add_method_when_user_is_authenticated()
    {
        $user = User::factory()->make([
            'password' => bcrypt($password = 'anona123'),
        ]);
        $response = $this->actingAs($user)->call('PUT', "/movies/add/tt10515848");
        $response->assertStatus(200);
    }

    public function test_it_returns_403_status_when_user_tries_to_add_same_movie_to_favourites_()
    {
        $user = User::factory()->make([
            "name"=> "Sneha",
            "email"=> "sneha@gmail.com",
            "email_verified_at"=> null,
            "favourite_movies"=> "tt10515848",
            "created_at"=> "2021-03-09T15:09:58.000000Z",
            "updated_at"=> "2021-03-14T17:41:49.000000Z",
            'password' => bcrypt($password = 'sneha123'),
        ]);
        $response = $this->actingAs($user)->call('PUT', "/movies/add/tt10515848");
        $response->assertStatus(403);
    }

    public function test_it_returns_302_status_for_remove_method_when_user_is_not_authenticated()
    {
        $response = $this->call('PUT', "/movies/remove/tt10515848");
        $response->assertStatus(302);
    }

    public function test_it_returns_200_status_for_remove_method_when_user_is_authenticated()
    {
        $user = User::factory()->make([
            "name"=> "Sneha",
            "email"=> "sneha@gmail.com",
            "email_verified_at"=> null,
            "favourite_movies"=> "tt10515848",
            "created_at"=> "2021-03-09T15:09:58.000000Z",
            "updated_at"=> "2021-03-14T17:41:49.000000Z",
            'password' => bcrypt($password = 'sneha123'),
        ]);
        $response = $this->actingAs($user)->call('PUT', "/movies/remove/tt10515848");
        $response->assertStatus(200);
    }
}

